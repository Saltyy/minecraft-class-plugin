package me.saltyy.abilityclasses.skills.skilldata.Spell;

public enum Spell {

    FIREBALL,SNOWBALL,ARROW,LIGHTNING,EGG,WATER,LAVA,FIRE,EXPLOSION,TRIDENT,TRANSMUTATION;

}
