package me.saltyy.abilityclasses.skills.skilldata.configdata;

public interface Cooldown {

    double getCooldown();
    String getCooldownMessage();

}
