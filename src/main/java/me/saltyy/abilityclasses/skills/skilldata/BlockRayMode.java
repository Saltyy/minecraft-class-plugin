package me.saltyy.abilityclasses.skills.skilldata;

public enum BlockRayMode {

    ABOVEBLOCK, CLOSERBLOCK, THEBLOCK

}
