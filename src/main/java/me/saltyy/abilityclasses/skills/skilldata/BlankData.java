package me.saltyy.abilityclasses.skills.skilldata;

import me.saltyy.abilityclasses.skills.Skill;
import org.bukkit.configuration.ConfigurationSection;

public class BlankData extends SkillData {
    protected BlankData(Skill skill, ConfigurationSection configurationSection) {
        super(skill, configurationSection);
    }
}
