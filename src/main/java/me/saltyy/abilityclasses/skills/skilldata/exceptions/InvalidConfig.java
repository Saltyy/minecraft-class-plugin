package me.saltyy.abilityclasses.skills.skilldata.exceptions;

public class InvalidConfig extends Exception {
    public InvalidConfig(String errorMessage) {
        super(errorMessage);
    }
}
